extern crate rustyline;
extern crate simple_error;
use console::rustyline::error::ReadlineError;
use simple_error::SimpleError;

pub const ERROR_ENTITY_OPENED_OPEN: &'static str = "Some entity is already open. Use 'apply' or 'discard' to close it.";
pub const ERROR_ENTITY_OPENED_REMOVE: &'static str = "Can't use 'remove' while some entity is still open. Use 'apply' or 'discard' to close it.";
pub const ERROR_INVALID_PARAMETER: &'static str = "Invalid Parameter.";
pub const ERROR_MISSING_PARAMETER_ID: &'static str = "Missing Parameter <id>.";
pub const ERROR_MISSING_PARAMETER_KEY: &'static str = "Missing Parameter <key>.";
pub const ERROR_MISSING_PARAMETER_VALUE: &'static str = "Missing Parameter <value>.";
pub const ERROR_NO_ENTITY_OPENED: &'static str = "No entity is opened. Use 'open <id>'.";
pub const ERROR_UNKNOWN_COMMAND: &'static str = "Unknown command. Use 'help' to show a list of available commands.";

pub enum Command {
    Apply,
    Create,
    Discard,
    Exit,
    Help,
    List,
    Load,
    None,
    Open(i64),
    Remove(i64),
    Reroll,
    Save,
    Set(String, String),
    Step,
}

pub enum State {
    Default,
    Open
}

pub struct Console {
    state: State,
    editor: rustyline::Editor<()>
}

impl Console {
    
    pub fn new() -> Console {
        Console {
             state: State::Default,
             editor: rustyline::Editor::<()>::new(),
         }
    }

    pub fn command(&mut self) -> Result<Command, SimpleError> {
        let input_text;
        let readline = self.editor.readline("> ");
        match readline {
            Ok(line) => {
                self.editor.add_history_entry(line.as_ref());
                input_text = line.trim().to_lowercase();
            },
            Err(ReadlineError::Interrupted) => {
                println!("CTRL-C");
                return Ok(Command::Exit)
            },
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
                return Ok(Command::Exit)
            },
            Err(err) => {
                bail!("{:?}", err);
            }
        }
        
        let commands: Vec<&str> = input_text.split_whitespace().collect();

        if commands.len() > 0 {
            match commands[0].as_ref() {
                "apply" => {
                    match self.state {
                        State::Default => bail!(ERROR_NO_ENTITY_OPENED),
                        State::Open => Ok(Command::Apply),
                    }
                },
                "create" => Ok(Command::Create),
                "discard" => {
                    match self.state {
                        State::Default => bail!(ERROR_NO_ENTITY_OPENED),
                        State::Open => Ok(Command::Discard),
                    }
                 },
                "exit" => Ok(Command::Exit),
                "help" => Ok(Command::Help),
                "list" => Ok(Command::List),
                "load" => Ok(Command::Load),
                "open" => {
                    match self.state {
                        State::Default => {
                            if commands.len() > 1 {
                                match commands[1].parse::<i64>() {
                                    Ok(_i) => Ok(Command::Open(_i)),
                                    Err(_err) => bail!(ERROR_INVALID_PARAMETER),
                                }
                            } else {
                                 bail!(ERROR_MISSING_PARAMETER_ID)
                            }
                        },
                        State::Open => bail!(ERROR_ENTITY_OPENED_OPEN),
                    }
                },
                "remove" => {
                    match self.state {
                        State::Default => {
                            if commands.len() > 1 {
                                match commands[1].parse::<i64>() {
                                    Ok(_i) => Ok(Command::Remove(_i)),
                                    Err(_err) => bail!(ERROR_INVALID_PARAMETER),
                                }
                            } else {
                                bail!(ERROR_MISSING_PARAMETER_ID)
                            }
                        }
                        State::Open => bail!(ERROR_ENTITY_OPENED_REMOVE)
                    }
                },
                "reroll" => {
                    match self.state {
                        State::Default => bail!(ERROR_NO_ENTITY_OPENED),
                        State::Open => Ok(Command::Reroll)
                    }
                },
                "save" => Ok(Command::Save),
                "set" => {
                    match self.state {
                        State::Default => Err(SimpleError::new(ERROR_NO_ENTITY_OPENED)),
                        State::Open => {
                            if commands.len() > 1 {
                                if commands.len() > 2 {
                                    Ok(Command::Set(commands[1].to_string(), commands[2].to_string()))
                                } else {
                                    bail!(ERROR_MISSING_PARAMETER_VALUE)
                                }
                            } else {
                                bail!(ERROR_MISSING_PARAMETER_KEY)
                            }
                        }
                    }
                },
                "step" => Ok(Command::Step),
                _ => bail!(ERROR_UNKNOWN_COMMAND),
            }
        } else {
            Ok(Command::None)
        }
    }

    pub fn on_apply(&mut self) {
        self.state = State::Default;
    }

    pub fn on_create(&self, _id: i64) {
        println!("Character has been created. Id: {}.", _id);
    }

    pub fn on_discard(&mut self) {
        self.state = State::Default;
    }
    
    pub fn on_error(&self, _error: &SimpleError) {
        println!("Error: {}", _error);
    }

    pub fn on_help(&self) {
        self.print_help("apply", "Apply the changes on the opened character and close it.");
        self.print_help("create", "Create a new character.");
        self.print_help("discard", "Discard the changes on the opened character and close it.");
        self.print_help("exit", "Exit the application.");
        self.print_help("help", "Print all available commands.");
        self.print_help("list", "List all characters.");
        self.print_help("load", "(Re)Load the database.");
        self.print_help("open <id>", "Open the character <id> for editing.");
        self.print_help("remove <id>", "Remove the character <id>.");
        self.print_help("reroll", "Rerolls the values of the opened character.");
        self.print_help("save", "Save into the database.");
        self.print_help("set <key> <value>", "Set <key> on the opened character to <value>.");
        self.print_help("step", "Simulate on step.");
    }

    pub fn on_load(&self) {
        println!("Database has been loaded.");
    }

    pub fn on_open(&mut self, _id: i64, _character: &str) {
        println!("Character with id '{}' has been opened.", _id);
        println!("{}", _character);
        self.state = State::Open
    }

    pub fn on_remove(&self, _id: i64) {
        println!("Character with id '{}' has been removed.", _id);
    }

    pub fn on_save(&self) {
        println!("Database has been saved.");
    }

    pub fn on_step(&self) {
        println!("Step!");
    }
    
    pub fn out(&self, _str: &str) {
        println!("{}", _str);
    }
    
    fn print_help(&self, _command: &str, _message: &str) {
        println!(" {:20}{}", _command, _message);
    }
}
