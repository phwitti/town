pub extern crate rand;
use data::rand::distributions::{Distribution, Range};
use data::{Character, Database};

pub fn simulate(_database: &mut Database) {
    morning(_database);
    afternoon(_database);
    evening(_database);
}

fn morning(_database: &mut Database) {
    let mut random = rand::thread_rng();
    let range = Range::new_inclusive(0.0f32, 1.0f32);
    let list = _database.get(&|_x| true);
    for mut x in list {
        x.arousal = range.sample(&mut random);
        x.pleasure = range.sample(&mut random);
        _database.apply(x);
    }
}

fn afternoon(_database: &mut Database) {
    let list = _database.get(&|_x| true);
    let mut to_bar = Vec::<Character>::new();
    let mut no_bar = Vec::<Character>::new();
    for mut x in list {
        let pleasure_to_extraversion = ((x.pleasure * 2.0f32 - 1.0f32) * x.arousal + 1.0f32) * 0.5f32;
        if pleasure_to_extraversion > (1.0f32 - x.extraversion) {
            to_bar.push(x);
        } else {
            no_bar.push(x);
        }
    }

    simulate_bar(_database, to_bar);
    simulate_home(_database, no_bar);
}

fn evening(_database: &mut Database) {
}

fn simulate_bar(_database: &mut Database, _character_at_bar: Vec<Character>) {
    for mut x in _character_at_bar {
        println!("to bar: {}", x.to_string());
        _database.apply(x);
    }
}

fn simulate_home(_database: &mut Database, _character_at_home: Vec<Character>) {
    for mut x in _character_at_home {
        simulate_home_single(_database, x);
    }
}

fn simulate_home_single(_database: &mut Database, _character: Character) {
    println!("no bar: {}", _character.to_string());
    _database.apply(_character);
}
