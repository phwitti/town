extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate simple_error;

mod console;
mod data;
mod simulation;

use simple_error::SimpleError;

//

fn main() {
    let mut database = data::Database::new("data/database.txt");
    let mut console = console::Console::new();
    let mut opt_opened_character: Option<data::Character> = None;
    load(&mut database, &console);

    loop
    {
        let result = console.command();
        match result {
            Ok(command) => {
                match command {
                    console::Command::Apply =>  {
                        apply(opt_opened_character, &mut database, &mut console);
                        opt_opened_character = None;
                    }
                    console::Command::Create => create(&mut database, &console),
                    console::Command::Discard => {
                        discard(&mut console);
                        opt_opened_character = None;
                    }
                    console::Command::Exit => break,
                    console::Command::Help => help(&console),
                    console::Command::List => list(&database, &console),
                    console::Command::Load => load(&mut database, &console),
                    console::Command::None => {},
                    console::Command::Open(_id) => {
                        opt_opened_character = open(_id, &database, &mut console);
                    },
                    console::Command::Remove(_id) => remove(_id, &mut database, &console),
                    console::Command::Reroll => reroll(opt_opened_character.as_mut(), &console),
                    console::Command::Save => save(&database, &console),
                    console::Command::Set(_key, _value) => set(&_key, &_value, opt_opened_character.as_mut(), &console),
                    console::Command::Step => step(&mut database, &console),
                }
            }
            Err(err) => error(&err, &console),
        }
    }
}

fn apply(_opt_character: Option<data::Character>, _database: &mut data::Database, _console: &mut console::Console) {
    match _opt_character {
        Some(character) => {
            _database.apply(character);
            _console.on_apply();
        }
        None => error(&SimpleError::new(console::ERROR_NO_ENTITY_OPENED), &_console),
    }
}

fn create(_database: &mut data::Database, _console: &console::Console) {
    let id = _database.create();
    _console.on_create(id);
}

fn discard(_console: &mut console::Console) {
    _console.on_discard();
}

fn error(_error: &SimpleError, _console: &console::Console) {
    _console.on_error(_error)
}

fn help(_console: &console::Console) {
    _console.on_help();
}

fn list(_database: &data::Database, _console: &console::Console) {
    let list = _database.list(&|_x| true);
    for x in &list {
        _console.out(&x.to_string());
    }
}

fn load(_database: &mut data::Database, _console: &console::Console) {
    match _database.load() {
        Ok(_) => _console.on_load(),
        Err(err) => error(&err, &_console)
    }
}

fn open(_id: i64, _database: &data::Database, _console: &mut console::Console) -> Option<data::Character> {
    let opt_character = _database.open(_id);
    match opt_character.as_ref() {
        Some(character) => {
            _console.on_open(_id, &character.to_string());
        }
        None => error(&SimpleError::new(console::ERROR_INVALID_PARAMETER), &_console),
    }
    return opt_character;
}

fn remove(_id: i64, _database: &mut data::Database, _console: &console::Console) {
    match _database.remove(_id) {
        Ok(_) => _console.on_remove(_id),
        Err(err) => error(&err, &_console)
    }
}

fn reroll(_opt_character: Option<&mut data::Character>, _console: &console::Console) {
    match _opt_character {
        Some(character) => {
            character.reroll();
            _console.out(&character.to_string());
        }
        None => error(&SimpleError::new(console::ERROR_NO_ENTITY_OPENED), &_console),
    }
}

fn save(_database: &data::Database, _console: &console::Console) {
    match _database.save() {
        Ok(_) => _console.on_save(),
        Err(err) => error(&err, &_console)
    }
}

fn set(_key: &str, _value: &str, _opt_character: Option<&mut data::Character>, _console: &console::Console) {
    match _opt_character {
        Some(character) => {
            match character.set(_key, _value) {
                Ok(_) => {}
                Err(err) => error(&err, &_console)
            }

            _console.out(&character.to_string());
        }
        None => error(&SimpleError::new(console::ERROR_NO_ENTITY_OPENED), &_console),
    }
}

fn step(_database: &mut data::Database, _console: &console::Console) {
    simulation::simulate(_database);
    _console.on_step();
}
