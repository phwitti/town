pub extern crate nalgebra;
pub extern crate rand;
pub extern crate serde_json;
pub extern crate simple_error;
use data::rand::distributions::{Distribution, Range};
use simple_error::SimpleError;
use std::fmt;
use std::fs;
use std::mem;
use std::path;

#[derive(Serialize, Deserialize, Clone)]
pub struct Character {
    pub id: i64,
    pub name: String,

    // static configuration
    pub openness: f32,
    pub conscientiousness: f32,
    pub extraversion: f32,
    pub agreeableness: f32,
    pub neuroticism: f32,

    // dynamic mood
    pub arousal: f32,
    pub pleasure: f32,

    // interests
    pub action: f32, // excitement // traveling, hiking, sports
    pub socializing: f32, // competition, community // sports, games, camping, family // helpfulness
    pub mastery: f32, // challenge, strategy // sports, music, singing // righteousness, knowledge
    pub achievement: f32, // sports, writing, hiking // power
    pub immersion: f32, // fantasy // traveling, reading, hiking, camping
    pub creativity: f32 // art, music, cooking/baking, gardening, photography, writing, painting // difference, quietness
}

impl Character {
    pub fn new(_id: i64) -> Character {        
        let mut character = Character {
            id: _id,
            name: "".to_string(),
            openness: 0f32,
            conscientiousness: 0f32,
            extraversion: 0f32,
            agreeableness: 0f32,
            neuroticism: 0f32,
            arousal: 0f32,
            pleasure: 0f32,
            action: 0f32,
            socializing: 0f32,
            mastery: 0f32,
            achievement: 0f32,
            immersion: 0f32,
            creativity: 0f32,
        };
        character.reroll();
        return character;
    }
    
    pub fn reroll(&mut self) {
        let mut random = rand::thread_rng();
        let mut range = Range::new_inclusive(0.0f32, 1.0f32);
        let vec_interests = nalgebra::normalize(
            &nalgebra::Vector6::from_distribution(&mut range, &mut random)
        );

        self.openness = range.sample(&mut random);
        self.conscientiousness = range.sample(&mut random);
        self.extraversion = range.sample(&mut random);
        self.agreeableness = range.sample(&mut random);
        self.neuroticism = range.sample(&mut random);
        
        self.arousal = range.sample(&mut random);
        self.pleasure = range.sample(&mut random);
        
        self.set_interests(&vec_interests);
    }

    pub fn set(&mut self, _key: &str, _value: &str) -> Result<(), SimpleError> {
        match _key {
            "id" => bail!("Access Denied. Cannot modify this value."),
            "name" => { self.name = _value.to_string(); Ok(()) }

            "openness" => Character::set_f32(&mut self.openness, _value),
            "conscientiousness" => Character::set_f32(&mut self.conscientiousness, _value),
            "extraversion" => Character::set_f32(&mut self.extraversion, _value),
            "agreeableness" => Character::set_f32(&mut self.agreeableness, _value),
            "neuroticism" => Character::set_f32(&mut self.neuroticism, _value),

            "arousal" => Character::set_f32(&mut self.arousal, _value),
            "pleasure" => Character::set_f32(&mut self.pleasure, _value),

            "action" => self.set_interest(0, _value),
            "socializing" => self.set_interest(1, _value),
            "mastery" => self.set_interest(2, _value),
            "achievement" => self.set_interest(3, _value),
            "immersion" => self.set_interest(4, _value),
            "creativity" => self.set_interest(5, _value),
            _ => bail!("Key not found."),
        }
    }

    fn set_f32(_key: &mut f32, _value: &str) -> Result<(), SimpleError> {
        let result = _value.parse::<f32>().and_then(
            |value| { *_key = nalgebra::clamp(value, 0f32, 1f32); Ok(()) }
        );
        try_with!(result, "Could not parse value.");
        Ok(())
    }
    
    fn set_interest(&mut self, _interest_index: usize, _value: &str) -> Result<(), SimpleError> {
        let result = _value.parse::<f32>();
        try_with!(result, "Could not parse value.");
        let value = nalgebra::clamp(result.unwrap(), 0f32, 1f32);
        let mut interests = self.get_interests();
        interests[_interest_index] = 0f32;
        interests = (1f32 - value) * nalgebra::normalize(&interests);
        interests[_interest_index] = value;
        self.set_interests(&interests);
        Ok(())
    }
    
    fn get_interests(&self) -> nalgebra::Vector6<f32> {
        nalgebra::Vector6::new(
            self.action,
            self.socializing,
            self.mastery,
            self.achievement,
            self.immersion,
            self.creativity
        )
    }
    
    fn set_interests(&mut self, _vector: &nalgebra::Vector6<f32>) {
        self.action = _vector[0];
        self.socializing = _vector[1];
        self.mastery = _vector[2];
        self.achievement = _vector[3];
        self.immersion = _vector[4];
        self.creativity = _vector[5];
    }
}

impl fmt::Display for Character {
    fn fmt(&self, _formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(_formatter, "( \
            id:{}, name:{}, openness:{}, conscientiousness:{}, extraversion:{}, \
            agreeableness:{}, neuroticism: {}, arousal:{}, pleasure:{}, action:{}, \
            socializing:{}, mastery:{}, achievement:{}, immersion:{}, creativity:{} )",
            self.id, self.name, self.openness, self.conscientiousness, self.extraversion,
            self.agreeableness, self.neuroticism, self.arousal, self.pleasure, self.action,
            self.socializing, self.mastery, self.achievement, self.immersion, self.creativity
        )
    }
}

pub struct Database {
    filename: String,
    data: Vec<Character>
}

impl Database {
    pub fn new(_filename: &str) -> Database {
        Database {
            filename: _filename.to_string(),
            data: Vec::<Character>::new()
        }
    }

    pub fn apply(&mut self, _character: Character) {
        mem::replace(
            &mut *self.data.iter_mut()
                           .filter(|x| x.id == _character.id)
                           .next()
                           .unwrap(), 
            _character
        );
    }

    pub fn create(&mut self) -> i64 {
        let mut id = 0;
        if self.data.len() != 0 {
            id = self.data[self.data.len() - 1].id + 1;
        }
        self.data.push(Character::new(id));
        return id;
    }

    pub fn list(&self, _filter: &Fn(&Character) -> bool) -> Vec<&Character> {
        self.data.iter().filter(|x| _filter(x)).collect()
    }

    pub fn get(&mut self, _filter: &Fn(&Character) -> bool) -> Vec<Character> {
        self.data.iter().filter(|x| _filter(x)).map(|x| (*x).clone()).collect()
    }

    pub fn load(&mut self) -> Result<(), SimpleError> {
        let read = fs::read(&self.filename);

        match read {
            Ok(vec) => {
                let string = String::from_utf8_lossy(&vec).to_string();
                let deserialized : Result<Vec<Character>, _> = serde_json::from_str(&string);
                match deserialized {
                    Ok(data) => {
                        self.data = data;
                        Ok(())
                    }
                    Err(_) => bail!("Failed to load the database. Invalid Format.")
                }
            }
            Err(_) => bail!("Failed to load the database.")
        }
    }

    pub fn open(&self, _id: i64) -> Option<Character> {
        let vec : Vec<&Character> = self.data.iter().filter(|x| x.id == _id).collect();
        let elem = vec.first();
        match elem {
            Some(character) => { Some((*character).clone()) }
            None => { None }
        }
    }

    pub fn remove(&mut self, _id: i64) -> Result<(), SimpleError> {
        let len_prev = self.data.len();
        self.data.retain(|x| x.id != _id);
        if len_prev == self.data.len() {
            bail!("Id not found in the database.")
        }
        else {
             Ok(())
        }
    }

    pub fn save(&self) -> Result<(), SimpleError> {
        let json = serde_json::to_string(&self.data).unwrap();
        let parent_path = path::Path::new(&self.filename).parent();
        match parent_path
        {
            Some(path) => {
                fs::DirBuilder::new()
                    .recursive(true)
                    .create(path)
                    .unwrap();
            }
            None => {}
        }

        try_with!(fs::write(&self.filename, &json), "Failed to save the database.");
        Ok(())
    }
}
